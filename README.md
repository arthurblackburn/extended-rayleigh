# Extended Rayleigh

Codes to explore the variation of particle diameter, signal to noise ratio and beam requirements for extended Rayleigh resolution measurements[1].

These Matlab codes allow users to determine the signal yield from a single or double particle system scanned with a Gaussian beam. The particles are considered to be either disc or sphere-like with a finite size that is normalized with respect to the beam width. This, for example, allows one to easily determine the mid-gap to peak intensity ratio, I_mid-pk, that occurs with arbitrary particle size and beam width, and determine I_mid-pk, that gives the best estimate of the equivalent true Rayleigh resolution, when finite sized particles are used.

FigureCreationScript.m contains code to generate figures used in a publication related to this research[1], and gives examples of how to use the related functions posted here.
CalculationExample.m works through the example calculation presented in Section 3 of [1].

Please cite the below [1] if this code or results arising from it are helpful for your work.

[1] Blackburn A M and Sasaki T, Particle diameter, Signal-to-Noise Ratio and Beam Requirements for Extended Rayleigh Resolution Measurements in the Scanning Electron Microscope, Microscopy 69, 248-257 (2020).

Arthur M. Blackburn, University of Victoria, 2020
