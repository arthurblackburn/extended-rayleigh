function [Imax, Imid, I2pmax_centre_offset] = I2p(Dp_norm, L_norm, particle_model) 
% Finds the maximum, central minimum intensity and peak intensity position 
% offset of two particles of diameeter Dp separated by L_norm, 
% centre-to-centre distance. Usage:
%
% [Imax, Imid, I2pmax_centre_offset] = I2pmax(Dp_norm, L_norm, particle_model) 
%
%
% 'disc': Simple approximation for large particle, when the measured 
% quantity is approximated as not related to particle thickness, e.g. 
% SE with no edge effect, light reflection, BSE etc.
%
% 'sphere': where signal is approximated as proportional to thickness of 
% particle, HAADF for example.
%
% Note: result is only valid when L_norm >= Dp_norm, as this model sums two
% single particle models. (Overlapped particles would thus create a regiob
% of double yield). Returns NaNs if this condition on input is not
% followed.
%
% @author: Arthur M. Blackburn, University of Victoria, ablackbu@uvic.ca
%
if nargin < 3
    particle_model = 'sphere';
end

if L_norm >= Dp_norm

    single_p = @(x_cent, x)  I1p(Dp_norm, abs(x - x_cent), particle_model);
    double_p = @(L_norm, x) single_p(-L_norm/2, x) + single_p(L_norm/2, x);

    % peak will surely be within the bounds of the particle and within 1/e full width:
    % search_x = (-0.5:0.001:0.5) + (L_norm/2);
    search_x = 0:0.001:((L_norm/2)+(Dp_norm/2));

    [Imax, Imax_ind] = max(double_p(L_norm,search_x));
    I2pmax_x = search_x(Imax_ind);
    I2pmax_centre_offset = (L_norm/2) - I2pmax_x ;

    Imid = double_p(L_norm, 0);
else
    Imax  = NaN;
    Imid  = NaN;
    I2pmax_centre_offset = NaN;    
end

end