function savedFiles = ComputeConvolutions(saveLocation, compMethod, models)
% Computes the convolution of Gaussian beam with a disc or spherical
% particle, saving the results into mat files that are called and used in
% later functions. Parameters such as range of Dp etc are currently
% embedded in the function.
%
% Usage:
%
% savedFiles = ComputeConvolutions(saveLocation, compMethod, models)
%
% savedFiles: cell array of files written out
% saveLocation: base directory to write to
% compMethod: computation method, either 'builtin' or 'trapz2d'.
%       trapz is much quicker than Matlab's builtin in integration method,
%       though generaly not as accurate. Use trapz before proceeding to
%       builtin method to check things out for example.
% models: either, 'sphere', 'disc' or 'all', i.e. which models to compute
% 
% (C) Arthur M. Blackburn, University of Victoria, ablackbu@uvic.ca
% 2019

if nargin < 3
    models = 'all';
end

if nargin < 2
    compMethod = 'builtin';
end

if nargin < 1
    saveLocation = pwd;
end


particle_models.sphere = @sphere_model;
particle_models.disc   = @disc_model;

if strcmpi(models, 'all')
    run_models = particle_models;
else
    if iscell(models)
        for ii = 1:numel(models)
            run_models.(models{ii}) = particle_models.(models{ii});
        end
    else
        run_models.(models) = particle_models.(models);
    end
end

savedFiles = PerformCalcs(run_models, saveLocation, compMethod);

end

function N = disc_model(X, Y, Dp)
% Simple disc model
     N = X.^2 + Y.^2 < (Dp/2).^2;
% note output is bool
end

function N = sphere_model(X, Y, Dp)
% Gives normalized projected thickness of shperhical particle with 
% diameter Dp at coordinates X,Y from its centre.
%
% Example Usage:
%
% Diameter = 180;
% xx = -100:1:100;
% [X, Y] = meshgrid(xx,xx);
% N = spherical_particle(X, Y, Diameter);
% figure; 
% surf(X, Y, N, 'LineStyle','None');

    Rp_sqrd = (Dp/2).^2;
    r_sqrd  = X.^2 + Y.^2;
    within_sphere =  r_sqrd < Rp_sqrd;

    N = zeros(size(X));
    N(within_sphere) = 2*sqrt(Rp_sqrd - r_sqrd(within_sphere))/Dp;

end

function  savedFiles = PerformCalcs(run_models, store_dir, comp_method)
% Compute convolution for spherical or disc model:
%
% particle_models is cell array of function handles for partilce models to
% be used.
%
% store_dir, directory where results are to be saved.
% 
% comp_method as described above in parent, simple trapezoidal approximation or
% built-in Matlab integral, either
% comp_method = 'builtin'; % or
% comp_method = 'trapz2d';
%
% Model of the beam, X, Y as the position in the plane
% beam = @(X, Y, d) 4*exp(-(4*X.^2+4*Y.^2)/d.^2)/(pi*d.^2);
% In all calculations all is normalized to the beam size, here
% characterized by the beam 1/e width: d. i.e. all is normalized to d = 1,
% as in referenced paper.
% i.e. d = 1; beam_normed = @(X, Y) beam(X, Y, d); thus simply:
beam_normed = @(X, Y) 4*exp(-(4*X.^2+4*Y.^2))/pi;

% When using a simple trapezoidal integration method, this is the 
% integration limit for the Gaussian beam, i.e. how many d to go out to.
d = 1;
integ_lim = 4*d;
% For trapezoidal method a factor between the step in x, the convolution
% direction, and the integration mesh. Must be an even number
simp_factor = 4;

trapz2d = @(X, grid_size) grid_size^2 * ...
          sum(sum( (X(1:(end-1),1:(end-1)) + X(2:end,2:end) + X(1:(end-1),(2:end)) + X(2:end,1:(end-1)) ) /4)) ;

if ~exist(store_dir,'dir')
    mkdir(store_dir);
end

% Here compute with particles out to Dp = 11*d 
% for x = 0 -> 10*d
% Calculation to 11 to allow safe interpolation around value of 10. Nothing
% to do with Spinal Tap ;)

model_names = fieldnames(run_models);
savedFiles = cell(1, length(model_names));

for modidx = 1:length(model_names)
particle = run_models.(model_names{modidx});

for ii = 1:2
    
    % Cover a large region in coarse steps
    if ii == 1
        Dp_max = 11; % Later use only to 10...
        x_step = 0.1;
        fname_suffix = '_coarse.mat';   
    end
    
    % The region where the particle size is 'small' has a bit more going on. 
    % Take smaller steps around here.
    if ii == 2
        Dp_max = 1;
        x_step = 0.025;
        fname_suffix = '_fine.mat';
             
    end
    fname = [ model_names{modidx},'_',comp_method ,'_I_1p',fname_suffix]; 
    Dp_arr = x_step: x_step : Dp_max; % this means there is overlap between fine and coarse ranges.
    xp_arr = 0:x_step:(Dp_max + integ_lim); % once outside of this limit, the integral will be zero.

    % Create mesh of points:
    simp_step = x_step/simp_factor;
    BeamRadiusIntervals = floor(integ_lim/simp_step);
    x_beam = simp_step*((-BeamRadiusIntervals):1:(BeamRadiusIntervals));
    y_beam = simp_step*(0:1:(BeamRadiusIntervals));
    [XX_beam, YY_beam] = meshgrid(x_beam, y_beam); 
    
    results = zeros(length(Dp_arr), length(xp_arr));

    for dp_ind = 1:length(Dp_arr)

        Dp = Dp_arr(dp_ind);

        conv_result_for_Dp = zeros(1,length(xp_arr));

        switch comp_method
            case 'builtin'
                % Use built-in integration method to evaluate convolution, sped up 
                % with parallel toolbox 'parfeval'. With 8 cores, using this method takes about 15 - 30 minutes.              

                for idx = 1:length(xp_arr)
                    F(idx) = parfeval(@ConvBeamAndParticle, 1, particle, beam_normed, Dp, xp_arr(idx), integ_lim); %#ok<AGROW>
                end

                conv_result_for_Dp = fetchOutputs(F)'; % Collects all results. Blocks till all done.
                conv_result_for_Dp( ~isreal(conv_result_for_Dp) ) = NaN;

                clear F; % subsequent calls may have xp_arr of reduced length...

            case 'trapz2d'            
                % Use simple trapezoidal method to do integrations (and hence
                % convolution). Is quicker than the above (takes <2 mins), but less
                % accurate than accurate integral method. Provides a quick
                % check if you want to try different particle or beam
                % moodels
                
                I_beam = beam_normed(XX_beam ,YY_beam);

                % The signal from outside the particle region is zero
                ParticleRadiusIntervals = floor((Dp/2)/simp_step);
                particle_lims = ParticleRadiusIntervals * simp_step;
                x_particle = (-particle_lims):simp_step:particle_lims;

                [XX_particle, YY_particle] = meshgrid(x_particle, y_beam);
                particle_z = particle(XX_particle ,YY_particle,Dp);

                for xp_ind = 1:length(xp_arr)
                     x_shift = xp_arr(xp_ind);
                     interval_shift = round(x_shift/simp_step);
                     % This should be a whole number, but round anyway just in 
                     % case of small rounding errors

                     % shift, crop particle if needed, multiply...
                     particle_left_crop = ParticleRadiusIntervals + interval_shift - BeamRadiusIntervals;
                     particle_right_crop = ParticleRadiusIntervals - interval_shift - BeamRadiusIntervals; 

                     if particle_left_crop > 0
                         crp_particle = particle_z(:,(1+particle_left_crop):end);
                         crp_beam = I_beam;
                     elseif particle_left_crop < 0
                         crp_particle = particle_z;
                         crp_beam = I_beam(:,(1+abs(particle_left_crop)):end);
                     else
                         crp_particle = particle_z;
                         crp_beam = I_beam;
                     end

                     if particle_right_crop > 0
                         crp_particle = crp_particle(:, 1:(end - particle_right_crop));                    
                     elseif particle_right_crop < 0                     
                         crp_beam = crp_beam(:,1:(end - abs(particle_right_crop)));
                     end

                     beam_particle_product = 2*crp_particle.*crp_beam; % Factor of 2 as above considered only a half plane                
                     % and integrate:                 
                     conv_result_for_Dp(xp_ind) = trapz2d( beam_particle_product, simp_step );

                end
            otherwise
                error('Unknown Comp Method: must be either builtin or trapz2d');
        end
        results(dp_ind,:) = conv_result_for_Dp;
%         disp(dp_ind); % crude progress indicator
    end
    savedFiles{ii} = fullfile(store_dir,fname);
    save(savedFiles{ii},'results','Dp_arr','xp_arr');
    
end
end

end

function op = ConvBeamAndParticle(particle_model, beam_model, Dp, xp, integ_limit)

    particle_i = @(x,y) particle_model(x, y, Dp);
    beam_i = @(x,y) beam_model(x, y);

    overfactor = 1.01; % just go a little bit over the particle edge to be sure...
    y_lim = min(Dp, integ_limit);
    x_min = max(-(xp + overfactor*(Dp/2)), -integ_limit);
    x_max = min(-(xp - overfactor*(Dp/2)),  integ_limit);

    kern_XPYP = @(x,y) particle_i(x+xp, y).*beam_i(x, y);

    op = 2*integral2(kern_XPYP, x_min, x_max, 0, y_lim,...
        'Method', 'iterated');
    % Factor of 2 as above considered only a half plane  

end
