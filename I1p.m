function II = I1p( Dp_norm, r_norm, particle_model, n0, np, store_dir, comp_method)
% Gives the intensity from convolving a 2-D Gaussian with a circular patch
% or spherical particle with yield np on a background with yield n0. 
%
% The beam intensity is represented by:
% S(r, d) = (4/(pi*d^2)) * exp( -4*r^2 / d^2), 
% where r is the radial distance from beam centre, and d is the 1/e full
% width of the Gaussian beam.
%
% Usage I = I1p(  Dp_norm, r_norm, particle_model, n0, np, store_dir, comp_method )
% 
% Dp_norm is the paricle diameter, divided by d.
%
% r_norm is radial distance from the centre of the particle at which the
% beam is placed, resulting in the intensity II.
% Both r_norm and Dp_norm can be 1-d row vectors (arrays).
%
% partcicle_model can be:
% 'disc': Simple approximation for large particle, when the measured 
% quantity is approximated as not related to particle thickness, e.g. 
% SE with no edge effect, light reflection, etc.
% OR
% 'sphere': where signal is approximated as proportional to thickness of 
% spherical particle
%
% n0 and np are the (bulk) backround and yields.
%
% store_dir
% is the directory where the previously computed results, which are
% interpolated by this function are stored. The default (also setting by
% setting store_dir=[]) is the folder where this function is stored.
%
% This function interpolates precomputed results. Compute these results by
% running ComputeConvolutions function. Within this the computation is
% carried out usign either trapz2d or builtin integration methods, which
% can be selected through comp_method, which currently defaults to
% 'builtin'. See also ComputeConvolutions.m.
%
% (C) Arthur M. Blackburn, University of Victoria, ablackbu@uvic.ca
% 2019

if nargin < 7
    % The integration method: simple trapezoidal approximation or built-in Matlab integral
    comp_method = 'builtin';
end
if (nargin < 6 ) || isempty(store_dir)
    % i.e. assume the file is in the same folder as this function.
    store_dir = fileparts(mfilename('fullpath')); 
end
if nargin < 5
    np = 1;
end
if nargin < 4
    n0 = 0;
end
if nargin < 3
    particle_model = 'sphere';
end

r_norm = abs(r_norm);

data_ranges = {'coarse','fine'};

persistent src loaded_model

if isempty(loaded_model) || (~strcmpi(loaded_model{1}, particle_model)) || (~strcmpi(loaded_model{2}, comp_method))
    mirror = @(x, reflected_sign) [sign(reflected_sign)*x(:,end:-1:2), x]; % assumes first column of x is the symmetry axis (=0)
    for ii = 1:2
        fname = [ particle_model,'_',comp_method ,'_I_1p_',data_ranges{ii},'.mat']; % same name from ComputeConvolutions
        src.(data_ranges{ii}) = load(fullfile(store_dir,fname));
        % Mirror to give better interpolation around symmetry axis:
        src.(data_ranges{ii}).xp_arr  = mirror(src.(data_ranges{ii}).xp_arr, -1);
        src.(data_ranges{ii}).results = mirror(src.(data_ranges{ii}).results, 1);
        [src.(data_ranges{ii}).X, src.(data_ranges{ii}).D] = ...
            meshgrid(src.(data_ranges{ii}).xp_arr, src.(data_ranges{ii}).Dp_arr);
    end
    loaded_model{1} = particle_model;
    loaded_model{2} = comp_method;
end

% Sort out masks for the ranges were Dp fall:
masks.Dpfine =   (min(src.fine.Dp_arr) < Dp_norm ) & (Dp_norm <= max(src.fine.Dp_arr));
masks.Dpcoarse = (max(src.fine.Dp_arr) < Dp_norm ) & (Dp_norm <= (max(src.coarse.Dp_arr) - 0.5));
% - 0.5 is to make sure we are well within range of the data for safeinterpolation
masks.Dpout_of_range = ~(masks.Dpfine | masks.Dpcoarse);
% ... could also do checking on r_range, but for now let interp take care
% of this.

[DD, RR] = meshgrid(Dp_norm, r_norm);
masks.fine   = repmat(masks.Dpfine,   length(r_norm), 1);
masks.coarse = repmat(masks.Dpcoarse, length(r_norm), 1);
masks.NaN    = repmat(masks.Dpout_of_range, length(r_norm), 1);
II = zeros(length(r_norm), length(Dp_norm));

for ii = 1:2 
    model_range = data_ranges{ii};
    II(masks.(model_range)) = interp2(src.(model_range).X, src.(model_range).D, src.(model_range).results, ...
        RR(masks.(model_range)), DD(masks.(model_range)), 'spline', 0);
end    

if any(masks.Dpout_of_range)
    warning('Some Dps values requested were out of range for interpolation. These were set to NaN.');
    II(masks.NaN) = NaN;
end

% apply the final scaling
II = ((np - n0)*II) + n0;

end

