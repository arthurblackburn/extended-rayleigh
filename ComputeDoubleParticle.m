function fileNames = ComputeDoubleParticle(fileLocations, models)
% Computes data for double particle system.
% This requires the single particle data has previously been calculated 
% and is stored in fileLocations.
%
% usage:
% fileNames = computeDoubleParticle(fileLocations, models)
%
% models is cell containing models to called. The names should be consistent 
% with those used to create single particle data.
%
% (C) Arthur M. Blackburn, University of Victoria, 2019

if nargin < 2
	models = {'disc', 'sphere'}
end

if nargin < 1
	fileLocations = pwd;
end

fileNames = cell(1,numel(models));

% Setup the ranges for the data. 
% Here we are working from Dp = 0.1 to 10.
Dp_rng_min = 0.1;
Dp_rng_max = 10;
D_min = log10(Dp_rng_min);
D_max = log10(Dp_rng_max);
D_steps = 100;
log_D_range = (D_min:((D_max-D_min)/(D_steps-1)):D_max);
Dp_range = 10.^log_D_range;

% .. and with gaps between 0 and 1.5 Dp (Lgap = L_minus_Dp)
L_minus_Dp_rng = 0:0.01:1.5;

% Calculation are done on this grid
[L_min_D_grd, ~] = meshgrid(L_minus_Dp_rng, log_D_range);

I_max = zeros(size(L_min_D_grd));     % The maximum value of the 2 particle system
I_mid = zeros(size(L_min_D_grd));     % The mid-gap value of the 2 particle system

% Note also the maximum value points in the two particle is not exactly at the 
% centre of the particles 
Pk_offset = zeros(size(L_min_D_grd));

for idx = 1:length(models)
particle_model = models{idx};
tic;
% do the calc
for D_ind = 1:length(Dp_range)
    D_i = Dp_range(D_ind);

    for L_ind = 1:length(L_minus_Dp_rng)
        L_i = L_minus_Dp_rng(L_ind)+D_i;
        if L_i < D_i
            L_i = D_i;
            % considering values of L less than Dp gives invalid results
            % we expect the 'end user' to know that, but here in this region, set it to
            % give the results as if the particles are just touching.
        end
        [I_max(D_ind, L_ind) , I_mid(D_ind, L_ind) , Pk_offset(D_ind, L_ind)] = ...
            I2p(D_i, L_i, particle_model);
    end
    
    fraction_done = D_ind/length(Dp_range);
    time_per_fraction = toc/fraction_done;
    expected_comp_time = (1 - fraction_done) * time_per_fraction;
    fprintf('%s: percent Done %3.0f, Elapsed time = %5.1f, expected time to completion %5.1f\n', ...
        particle_model, fraction_done*100, toc, expected_comp_time);
    
end

% We call it double particle 'clipped' just remind us that were are only looking over range L_minus_Dp,
% i.e. we have no valid data for when L < Dp
fname = ['Double_particle_clipped_',particle_model,'.mat'];
fileNames{idx} = fullfile(fileLocations,fname);

save(['Double_particle_clipped_',particle_model,'.mat'],...
      'I_max','I_mid', 'Pk_offset', 'L_minus_Dp_rng', 'log_D_range');
end      

end