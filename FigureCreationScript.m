% This example file works through generating plots of the single and double 
% particle properties in Figures 3, 5 & 6-9 of 
% "Particle Diameter, Signal to Noise Ratio and Beam Requirements for 
% Extended Rayleigh Resolution Measurements in the Scanning Electron 
% Microscope"
%
% To provide a contour plot as per the paper set cont_plot = true
% To give a '3D' plot set false.
% cont_plot = true; % As in the paper
cont_plot = false; % To give '3D' surface plots

%% Run each cell as directed and if required
% 
%
% Compute the single particle convolutions, if they are not already determined
% and stored on file:

% savedFilesTrapz = ComputeConvolutions(pwd, 'trapz2d', 'all');
% savedFilesBuildIn = ComputeConvolutions(pwd, 'builtin', 'all');
% The latter might take ~10 mins.
%% Figure 3
% Sample data for the one particle system, requires .mat files generated 
% by the above:
% As in Figure 3(a)
x = 0:0.05:2;
Dp = [0.5, 1, 1.5, 2];

I_spherical = I1p(Dp, x, 'sphere');
I_disc = I1p(Dp, x, 'disc');
% Peak normalize and plot:
I_spherical_pknorm = I_spherical./I_spherical(1,:);
I_disc_pknorm = I_disc./I_disc(1,:);
%
figure; plot(x, I_spherical_pknorm);
hold on; plot(x, I_disc_pknorm, '--');
xlabel('rp/d'); ylabel('Peak normed intensity');
legcell = cell(1,8);
idx = 0; leg_titles = {'Sphere', 'Disc'};
for ii = 1:2
    for jj = 1:length(Dp)
        idx = idx + 1; 
        legcell{idx} = sprintf('%s Dp=%2.1f',leg_titles{ii},Dp(jj));
    end
end
legend(legcell,'Location','NorthEast');
%% 
% The max values in the inset of Figure 3(a)
Dp_for_max  = 0.05:0.05:2;
x_for_max = 0;
I_max_sphere = I1p(Dp_for_max, x_for_max, 'sphere');
I_max_disc = I1p(Dp_for_max, x_for_max, 'disc');
figure;
plot(Dp_for_max,I_max_sphere,'rx'); hold on;
plot(Dp_for_max,I_max_disc,'bx');
xlabel('Dp/d'); ylabel('max(eta1p)');

% Compare to the analytic functions;
disc_max = @(Dp_norm) 1 - exp(-Dp_norm.^2);
xc=5.7; %cut for asymptotic approximation (when x is real); from Per Sundqvist, Matlab FileExchange 
Erfi = @(x) ~isreal(x).*(-(sqrt(-x.^2)./(x+isreal(x))).*gammainc(-x.^2,1/2))+...
       isreal(x).*real(-sqrt(-1).*sign(x).*((x<xc).*gammainc(-x.^2,1/2))+...
       (x>=xc).*exp(x.^2)./x/sqrt(pi));
Dawson = @(x) (sqrt(pi)/2)*exp(-x.^2).*Erfi(x);
sphere_max = @(Dp_norm) 1 - Dawson(Dp_norm)./Dp_norm;

plot(Dp_for_max, disc_max(Dp_for_max), 'b--');
plot(Dp_for_max, sphere_max(Dp_for_max), 'r');

legend({'sphere numeric','disc numeric','sphere analytic','disc analytic'}, 'Location', 'NorthWest');

%%
% Plots for larger particle sizes, Figure 3(b)
Dp = [2, 4, 6, 8];
x = 0:0.05:9;

I_spherical = I1p(Dp, x, 'sphere');
I_disc = I1p(Dp, x, 'disc');
% Peak normalize and plot:
I_spherical_pknorm = I_spherical./I_spherical(1,:);
I_disc_pknorm = I_disc./I_disc(1,:);

figure;
legcell = cell(1,8);
idx = 0; leg_titles = {'Sphere', 'Disc'};
for ii = 1:2
    if ii == 1
        I = I_spherical_pknorm;
        ltype = '-';
    else
        I = I_disc_pknorm;
        ltype = '--';
    end
    for jj = 1:length(Dp)
        II = I(:,jj)';
        x_centre_elem = find(x == Dp(jj)/2,1,'first');
        x_display = x - Dp(jj)/2;
        display_mask = (x_display >= -1)&(x_display<=1);
        plot(x_display(display_mask), II(display_mask), ltype); hold on
        idx = idx + 1; 
        legcell{idx} = sprintf('%s Dp=%2.1f',leg_titles{ii},Dp(jj));
    end
end
legend(legcell,'Location','NorthEast');
xlabel('x - Dp/2');
ylabel('Peak normed intensity');

%% Then compute double particle parameters, if they are not already stored
% in Double_particle_clipped_disc.mat ans Double_particle_clipped_sphere.mat
% fileNames = ComputeDoubleParticle();

%% Figure 5 and Figure 6
% Example Plots,
% e.g. as Fig 5/6(a);

% below is a fix to put Rayleigh line slighty above the surface for display
% purposes
display_offset = 0.01;


for fig_ind = [5,6]
%
if fig_ind == 5
    plot_param = 'mid_ratio'; title_leader = 'T'; % for Figure 5;
else
    % Re-run this cell with the below uncommented to make Figure 6
    plot_param = 'i_max'; title_leader = '\Gamma_{(peak)}';% for Figure 6;
end
%

Dp = 0.1:0.025:1.5;
L = 0.9:0.025:1.5;
[DD, LL] = meshgrid(Dp, L);
L_gap = LL - DD;
DD_reshape = DD(:);
L_gap_reshape = L_gap(:);

T_sphere = TwoParticleParams(plot_param, DD_reshape, L_gap_reshape, [], 'sphere');
T_sphere = reshape(T_sphere, size(L_gap));
T_disc = TwoParticleParams(plot_param, DD_reshape, L_gap_reshape, [], 'disc');
T_disc = reshape(T_disc, size(L_gap));

figure;
if cont_plot
    subplot(1,2,1);
    [C1,h1] = contour(DD, LL, T_sphere,'k'); hold on
    clabel(C1, h1);
    [C2,h2] = contour(DD, LL, T_disc,'k--');
    rayleigh_line = [0.1, 1.5; 1  1];
    plot(rayleigh_line(1,:),rayleigh_line(2,:),'r','LineWidth', 1);
    title({['(solid contours):',title_leader,'_{sphere} and'],['(dashed contours):', title_leader,'_{disc}']});
    xlabel('D_p/d');
    ylabel('L');
else
    subplot(2,2,1);
    surf(DD, LL, T_sphere,'LineStyle','none'); hold on
    title([title_leader,'_{sphere}'])
    x_rayleigh = 0.1:0.02:1.5;
    y_rayleigh = ones(size(x_rayleigh));
    rayleigh_line = interp2(DD, LL, T_sphere, x_rayleigh, y_rayleigh);
    plot3(x_rayleigh, y_rayleigh, rayleigh_line + display_offset,'r');
    [C1,h1] = contour3(DD, LL, T_sphere,'k');
    xlabel('D_p/d');
    ylabel('L');
    subplot(2,2,3);
    surf(DD, LL, T_disc,'LineStyle','none'); hold on;
    rayleigh_line = interp2(DD, LL, T_disc, x_rayleigh, y_rayleigh);
    plot3(x_rayleigh, y_rayleigh, rayleigh_line + display_offset,'r');
    [C2,h2] = contour3(DD, LL, T_disc,'k--');
    title([title_leader,'_{disc}'])
    xlabel('D_p/d');
    ylabel('L/d');    
end
% clabel(C2, h2);

% plot as Figure 5(b);
Dp = logspace(-1, 1, 50);
L_gap = 0:0.025:1.5;
[DD, LLGAP] = meshgrid(Dp, L_gap);
DD_reshape = DD(:);
L_gap_reshape = LLGAP(:);

T_sphere_bigrange = TwoParticleParams(plot_param, DD_reshape, L_gap_reshape, [], 'sphere');
T_sphere_bigrange = reshape(T_sphere_bigrange, size(DD));
T_disc_bigrange = TwoParticleParams(plot_param, DD_reshape, L_gap_reshape, [], 'disc');
T_disc_bigrange = reshape(T_disc_bigrange, size(DD));

ray_arg = (1 - L_gap); 
ray_arg(ray_arg<0) = 0;
ral = log10(ray_arg);

if cont_plot
    subplot(1,2,2);
    [C3,h3] = contour(log10(DD), LLGAP, T_sphere_bigrange,'k'); hold on
    clabel(C3, h3);
    [C4,h4] = contour(log10(DD), LLGAP, T_disc_bigrange,'k--');
    plot(ral, L_gap, 'r','LineWidth', 1);
    title({['(solid contours):',title_leader,'_{sphere} and'],['(dashed contours):', title_leader,'_{disc}']});
    xlabel('log_{10}(D_p/d)');
    ylabel('L_{gap}/d');
else
    subplot(2,2,2);
    surf(log10(DD), LLGAP, T_sphere_bigrange,'LineStyle','none'); hold on;
    rayleigh_line = interp2(log10(DD), LLGAP, T_sphere_bigrange, ral, L_gap);
    plot3(ral, L_gap, rayleigh_line + display_offset,'r');
    [C3,h3] = contour3(log10(DD), LLGAP, T_sphere_bigrange ,'k'); hold on
    title([title_leader,'_{sphere}'])
    xlabel('log_{10}(D_p/d)');
    ylabel('L_{gap}/d');
    subplot(2,2,4);
    if strcmpi(plot_param,'i_max')
        xlim([-1 0.5]);
    else
        xlim([-1 1]);
    end
    surf(log10(DD), LLGAP, T_disc_bigrange,'LineStyle','none'); hold on;
    rayleigh_line = interp2(log10(DD), LLGAP, T_disc_bigrange, ral, L_gap);
    plot3(ral, L_gap, rayleigh_line + display_offset,'r');
    [C4,h4] = contour3(log10(DD), LLGAP, T_disc_bigrange,'k--');
    title([title_leader,'_{disc}'])
    xlabel('log_{10}(D_p/d)');
    ylabel('L_{gap}/d');       
end

if strcmpi(plot_param,'i_max')
    xlim([-1 0.5]);
else
    xlim([-1 1]);
end

end
% clabel(C4, h4);
%% Figure 5(c) :
% "Mid-Gap to Peak Intensity Ratio (T) for Best Estimate of True Rayleigh Resolution L = d"

Dp = 0.1:0.01:0.99; Lgap = 1-Dp; T_disc = TwoParticleParams('mid_ratio',Dp, Lgap,[], 'disc');
Dp = 0.1:0.01:0.99; Lgap = 1-Dp; T_sphere = TwoParticleParams('mid_ratio',Dp, Lgap, [], 'sphere');

ff = figure; 
plot(Dp, T_disc, 'r'); hold on; plot(Dp, T_sphere, 'b');
title_props = {'FontName','Times New Roman','FontSize',10};
ax = gca;
set(ax, title_props{:}, 'LineWidth', 1);
leg_cell = {'Disc Model','Sphere Model'};
legend(leg_cell,'Location','northwest');
xlim([0.1 1]);
ylim([0.7 1]);
xlabel({'Normalized Particle Diameter';'Dp/d'});
ylabel({'Mid-Gap to Peak Intensity Ratio (T) for';'Best Estimate of True Rayleigh Resolution L = d'});

%% Figure 7
Dp = logspace(-1, 1, 50);
L_gap = 0:0.025:1.5;
[DD, LLGAP] = meshgrid(Dp, L_gap);
DD_reshape = DD(:);
L_gap_reshape = LLGAP(:);
kp = 5;

CNR_per_root_N0_sphere_example = TwoParticleParams('cnr_root_n0', DD_reshape, L_gap_reshape, kp, 'sphere');
CNR_per_root_N0_sphere_example = reshape(CNR_per_root_N0_sphere_example, size(DD));

figure;
if ~cont_plot
    surf(log10(DD), LLGAP, CNR_per_root_N0_sphere_example,'LineStyle','none'); hold on
    ray_DP_range = logspace(-1,log10(2),30);
    [~, L_gap_T0p72] = TwoParticleParams('i_mid', ray_DP_range, 'ray');
    z_pnts = interp2(log10(DD), LLGAP, CNR_per_root_N0_sphere_example, log10(ray_DP_range), L_gap_T0p72);
    plot3(log10(ray_DP_range), L_gap_T0p72, z_pnts + display_offset, 'r', 'LineWidth',2);
    [C_fig7,h_fig7] = contour3(log10(DD), LLGAP, CNR_per_root_N0_sphere_example, cont_levels, 'k'); hold on
else
    cont_levels = [0.025,0.05,0.1,0.2,0.3:0.2:2.1];
    [C_fig7,h_fig7] = contour(log10(DD), LLGAP, CNR_per_root_N0_sphere_example, cont_levels, 'k'); hold on
    clabel(C_fig7, h_fig7);
    % add the T = 0.72 line contour:
    ray_DP_range = logspace(-1,log10(2),30);
    [~, L_gap_T0p72] = TwoParticleParams('i_mid', ray_DP_range, 'ray');
    plot(log10(ray_DP_range), L_gap_T0p72, 'b');
end

xlabel('D_p');
ylabel('L - D_p');
title(['CNR / sqrt(N_0);  K_p = ', num2str(kp, 2)]);


%% Figure 8

kk = 1.1:0.1:10;
Dp = logspace(-1, log10(1.5), 50);
[DD, KK] = meshgrid(Dp, kk);
ZZ = zeros(size(DD));

for ki = 1:length(kk)
    ZZ(ki,:) = TwoParticleParams('cnr_root_n0', DD(ki,:), 'ray', kk(ki), 'sphere');
end

r2 = logspace(log10(0.01),  log10(2), 30);
r1 = logspace(log10(0.001), log10(r2(1)), 10); r1 = r1(1:(end-1));
cont_levels = round([r1, r2],3,'significant');

figure;
if ~cont_plot
    surf(log10(DD), KK, ZZ,'LineStyle','none'); hold on;
    [C_fig8,h_fig8] = contour3(log10(DD), KK, ZZ, cont_levels, 'k');
else
    [C_fig8,h_fig8] = contour(log10(DD), KK, ZZ, cont_levels, 'k');
    clabel(C_fig8,h_fig8);
end
xlabel('log(D_p)');
ylabel('k_p');
title('CNR / root(N_0)');


%% Figure 9

CNR_div_N0 = sqrt((2*(KK - 1).^2)./(KK + 1));
ratio_mat = CNR_div_N0./ZZ;

conts_fig9 = [0.75,0.8:0.1:2.1];

figure;
if ~cont_plot
    surf(log10(DD), KK, log10(ratio_mat),'LineStyle','none'); hold on;
    [C_fig9,h_fig9] = contour3(log10(DD), KK, log10(ratio_mat), conts_fig9, 'k');
else
    [C_fig9,h_fig9] = contour(log10(DD), KK, log10(ratio_mat), conts_fig9, 'k');
    clabel(C_fig9,h_fig9);
end
xlim([-0.7 0.17]);
xlabel('log(D_p)');
ylabel('k_p');
title('log_{10}[ CNR_{LA} / CNR_{2p max-mid}]');


