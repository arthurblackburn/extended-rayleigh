function [val, L_gap] = TwoParticleParams(param, Dp_d, L_gap, kp,  particle_model)
% Gives peak, mid-gap and other intensity related paramters for two
% paricle system
%
% Usage:
% [val, L_gap] = TwoParticleParams(param, Dp_d, L_gap, kp,  particle_model)
%
% param can be:
%   'i_max'
% The peak intensity of image of the two particle system, when scanned by
% Gaussian beam.
%   'mid_ratio'
% The mid-gap to peak intensity ratio.
%   'i_mid'
% The mid-gap intensity (non-normalized)
%   'pk_offset'
% The positional offset of the position of peak intensity from the centre
% of the particle
%   'cnr_root_n0'
% For a given kp, where kp = np/no, gives CNR_pk-mid/ root(N0), as in
% the Eq 11 of paper. CNR_pk-mid is the Contrast to Noise Ratio related to
% intensity peak and mid-gap intensity levels.
%
% Dp_d is the normalized particle diameter.
% L_gap is the normalized gap between the particles, where normalization 
% means division by the beam 1/e full width.
%
% Alternatively L_gap can be specified as 'Ray', in which case L_gap
% will be determined from the specified Dp_d such as to satisfy T = 0.72,
% where T is the mid-gap to peak intensity ratio. This is the
% characteristic that consitutes the general Extended Rayleigh Criterion.
% 
% kp, when specified (it can be set as []), is used in determining 
% 'cnr_root_n0'. kp = np/no, see the paper.
% 
% particle_model, is the particle model currently either 'disc' or
% 'sphere'. Defaults to 'sphere'.
%
% @author: Arthur M. Blackburn, University of Victoria, ablackbu@uvic.ca
% (C) Arthur M. Blackburn 2019

if nargin < 5
    particle_model = 'sphere';
end

if nargin < 4
    kp = [];
end

persistent dat L_min_D_grd log_D_grd rayleigh_XY loaded_model

inequal_tol = 1e-5;

if isempty(loaded_model) || (~strcmpi(loaded_model, particle_model))

    % Load the data
    dat = load(['Double_particle_clipped_',particle_model,'.mat'],...
        'I_max','I_mid', 'Pk_offset', 'L_minus_Dp_rng', 'log_D_range');  
    dat.mid_ratio = dat.I_mid./dat.I_max;
    dat.mid_ratio(dat.mid_ratio >= (1-inequal_tol )) = NaN;
    loaded_model = particle_model;
        
    % extract the T = 0.72 contour
    [L_min_D_grd, log_D_grd] = meshgrid(dat.L_minus_Dp_rng, dat.log_D_range);
    lev = 0.72;
    rayleigh_XY = contourc(dat.L_minus_Dp_rng, dat.log_D_range, dat.mid_ratio, [ lev,  lev]);
    interp1(rayleigh_XY(2,2:end),rayleigh_XY(1,2:end),log10(Dp_d));
end

log_D = log10(Dp_d);
if ~ischar(L_gap)
    L_min_D = L_gap;
else
    if strcmpi(L_gap,'ray')
        L_min_D = interp1(rayleigh_XY(2,2:end),rayleigh_XY(1,2:end),log10(Dp_d));
        L_gap = L_min_D;
    else
        error('unknown gap specification');
    end
end

switch lower(param)
    case 'i_max'
        val = interp2(L_min_D_grd, log_D_grd, dat.I_max, L_min_D, log_D);        
    case 'mid_ratio'
        val = interp2(L_min_D_grd, log_D_grd, dat.mid_ratio, L_min_D, log_D);
    case 'i_mid'
        val = interp2(L_min_D_grd, log_D_grd, dat.I_mid, L_min_D, log_D);
    case 'pk_offset'
        val = interp2(L_min_D_grd, log_D_grd, dat.Pk_offset, L_min_D, log_D);        
    case 'cnr_root_n0'
        f1 = interp2(L_min_D_grd, log_D_grd, dat.mid_ratio, L_min_D, log_D);
        f2 = interp2(L_min_D_grd, log_D_grd, dat.I_max, L_min_D, log_D);
        val = sqrt( (2* (f2 - f1 .* f2 ).^2 .* (kp - 1)) ./ ...
                    (f1 .* f2 + f2 + (2 / (kp - 1 )) ) );
    otherwise
        error('Unknown parameter');
end

end