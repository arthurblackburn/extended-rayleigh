%% Example values used in paper
% Refer to
% Electron-specimen interactions in low-voltage scanning electron microscopy. Scanning 1993, 15(1): 1-18.
% Figure 14, for Gold and Carbon
%% 
eta_Au = 1.200; % Gold
eta_C = 0.527; % Carbon
%% Section 2.3, first paragraph
t_pixel = 10e-6;
gamma = 0.5;
I_beam = 20e-12;
ec = 1.6022e-19;

N_Au = I_beam * eta_Au * t_pixel * gamma / ec;
N_C = I_beam * eta_C * t_pixel * gamma / ec;
CNR_LA = sqrt(2*((N_Au - N_C)^2)/(N_Au + N_C));

N_Au = round(N_Au, 3, 'significant')
N_C = round(N_C, 3, 'significant')
CNR_LA = round(CNR_LA, 3, 'significant')
%% Section 2.3, second paragraph
Dp = 1.5;
d = 2;
Dp_d = Dp/d;
f1 = 0.72;
[f2, L_gap_example1] = TwoParticleParams('i_max', Dp_d, 'ray');
separation_d = Dp_d + L_gap_example1;


separation = round(separation_d, 4, 'significant')
separation_nm = round(separation_d * d,  3, 'significant')

f2 = round(f2, 3, 'significant')
I2p_max = eta_C + (f2 * (eta_Au   - eta_C));
I2p_max_display = round(I2p_max, 3, 'significant')


I2p_mid = eta_C + (f1 * (I2p_max - eta_C));
I2p_mid_display = round(I2p_mid, 3, 'significant')

N2p_max = (I2p_max * I_beam) * t_pixel * gamma / ec;
N2p_max_display = round(N2p_max, 3, 'significant')

N2p_mid = (I2p_mid * I_beam) * t_pixel * gamma / ec;
N2p_mid_display = round(N2p_mid, 3, 'significant')

CNR_2p = sqrt(2*((N2p_max - N2p_mid)^2)/(N2p_max + N2p_mid));
CNR_2p_display = round(CNR_2p, 3, 'significant')

%% Section 2.3, paragraph 3

I2_ejected_max = I2p_max * I_beam;
I2_ejected_mid = I2p_mid * I_beam;

root_inc_ratio = 5 / CNR_2p;

inc_ratio = root_inc_ratio^2;
inc_ratio_disp = round(inc_ratio, 2, 'significant')

new_t_pixel = 20e-6;
inc_pix_time = new_t_pixel/t_pixel;
inc_current = inc_ratio / inc_pix_time;
new_current = round(I_beam * inc_current/1e-12,  2, 'significant')
%% Section 2.4, General Process
kp = eta_Au / eta_C

[CNR_root_N0, L_gap_example2] = TwoParticleParams('cnr_root_n0', Dp_d, 'ray', kp)

required_N0_from_handcalc = N_C * inc_ratio
ROUND_required_N0_from_fig9 = round((5/CNR_root_N0)^2,3,'significant')
ROUND_required_N0_from_handcalc = round(N_C*inc_ratio,3,'significant')






